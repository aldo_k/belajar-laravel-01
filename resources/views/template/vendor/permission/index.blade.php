<x-template-layout>
  <x-slot name="title">Select Role</x-slot>
    
  <x-template-datatables :fields="$fields" :options="$options" />

</x-template-layout>