<x-template-layout>
  <x-slot name="title">{{ $title ?? null }}</x-slot>
  
  <x-slot name="buttons">
    {!! $buttons ?? null !!}
  </x-slot>

  <x-template-datatables :fields="$fields" :options="$options" />

</x-template-layout>