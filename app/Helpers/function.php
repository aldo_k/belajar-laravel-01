<?php

use App\Helpers\Admin;

if(! function_exists( 'admin' ) ) {
  function admin() {
    return new Admin;
  }
}