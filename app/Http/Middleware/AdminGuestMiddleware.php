<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminGuestMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        if(auth()->guard(config('admin.auth.guard', 'web'))->check()) {
            return redirect()->route('administrator.index');
        }

        Auth::shouldUse(config('admin.auth.guard', 'web'));
        return $next($request);
    }
}
