<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AdminLogable;
use App\DataTables\AdminLogableDatatables;

class AdminLogableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        if(in_array(env('APP_ENV', 'local'), ['local'])) {
            session()->flash('warning', [
                'Add this trait <code>App\Models\AdminLogable</code> to all the Models you want to monitor'
            ]);
        }

        return AdminLogableDatatables::view();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $life = config('admin.log_activity_life', 7);
        $data = AdminLogable::where('created_at', '<', now()->addDays('-' . $life)->format('Y-m-d h:i:s'));
        $count = $data->count();
        $data->delete();

        if($count > 0) {
            session()->flash('success', [
                $count . ' has been deleted'
            ]);
        } else {
            session()->flash('success', [
                'No data available'
            ]);
        }


        return redirect()->back();
    }
}
